var Gara = /** @class */ (function () {
    function Gara() {
        var _this = this;
        this.div_descrizioni = $("#descrizioni");
        this.pulsante = $("#start input");
        console.log("New Gara");
        this.pulsante.on("click", function () { return _this.clickPulsante(); });
        this.initAtleti();
        this.fillDescrizioni();
    }
    Gara.prototype.generaNumeroCasuale = function (minimo, massimo, decimali) {
        if (decimali === void 0) { decimali = 0; }
        var out = NaN;
        if (typeof decimali === "undefined") {
            decimali = 0;
        }
        var fattore_decimali = Math.pow(10, decimali);
        var diff = (massimo * fattore_decimali) - (minimo * fattore_decimali);
        var numero = Math.random();
        var var_casuale = Math.round(numero * diff);
        out = (((minimo * fattore_decimali) + var_casuale) / fattore_decimali);
        return out;
    };
    Gara.prototype.initAtleti = function () {
        var eta;
        var velocita;
        eta = this.generaNumeroCasuale(20, 50);
        velocita = this.generaNumeroCasuale(5, 6, 2);
        this.atleta_1 = new Atleta("Giovanni", eta, velocita, "#gara .atleta1");
        eta = this.generaNumeroCasuale(20, 50);
        velocita = this.generaNumeroCasuale(5, 6, 2);
        this.atleta_2 = new Atleta("Andrea", eta, velocita, "#gara .atleta2");
        eta = this.generaNumeroCasuale(20, 50);
        velocita = this.generaNumeroCasuale(5, 6, 2);
        this.atleta_3 = new Atleta("Anna", eta, velocita, "#gara .atleta3");
        this.atleti = [this.atleta_1, this.atleta_2, this.atleta_3];
    };
    Gara.prototype.fillDescrizioni = function () {
        for (var i = 0; i < this.atleti.length; i++) {
            var atleta = this.atleti[i];
            var div = this.div_descrizioni.find(".atleta" + (i + 1));
            div.html(atleta.getProfile());
        }
    };
    Gara.prototype.clickPulsante = function () {
        var _this = this;
        this.interval_id = setInterval(function () { return _this.updateAtleti(); }, 500);
        this.pulsante.off("click");
    };
    Gara.prototype.updateAtleti = function () {
        var _this = this;
        var gara_finita = false;
        for (var i = 0; i < this.atleti.length; i++) {
            this.atleti[i].corri(10);
            if (this.atleti[i].metri_percorsi_totali >= 500) {
                gara_finita = true;
            }
        }
        if (gara_finita) {
            clearInterval(this.interval_id);
            this.pulsante.on("click", function () { return _this.riparti(); });
        }
    };
    Gara.prototype.riparti = function () {
        var _this = this;
        this.reset();
        this.interval_id = setInterval(function () { return _this.updateAtleti(); }, 500);
        this.pulsante.off("click");
    };
    Gara.prototype.reset = function () {
        this.initAtleti();
        this.fillDescrizioni();
    };
    return Gara;
}());
var gara = new Gara();
//# sourceMappingURL=Gara.js.map