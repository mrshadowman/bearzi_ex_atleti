class Persona {
    protected name: string;
    protected age: number;

    constructor (name:string, eta:number) {
        this.name = name;
        this.age = eta;
    }

    public getProfile ():string {
        var out:string = this.name + " è una persona di " + this.age + " anni";
        return out;
    }
}
