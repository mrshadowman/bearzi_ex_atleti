class Gara {
    private div_descrizioni: JQuery;
    private pulsante: JQuery;
    private interval_id: number;
    private atleta_1: Atleta;
    private atleta_2: Atleta;
    private atleta_3: Atleta;
    private atleti: Atleta[];
    private metri_percorsi_totali: number;

    constructor () {
        this.div_descrizioni = $("#descrizioni");
        this.pulsante = $("#start input");
        console.log("New Gara");
        this.pulsante.on("click",()=> this.clickPulsante());
        this.initAtleti();
        this.fillDescrizioni();
    }

    private generaNumeroCasuale(minimo:number ,massimo:number ,decimali:number=0):number {
        var out:number = NaN;
        if (typeof decimali === "undefined") {
            decimali = 0;
        }
        var fattore_decimali:number = Math.pow(10,decimali);
        var diff:number = (massimo*fattore_decimali)-(minimo*fattore_decimali);
        var numero:number = Math.random();
        var var_casuale:number = Math.round(numero*diff);
        out = (((minimo*fattore_decimali) + var_casuale)/fattore_decimali);
        return out;
    }

    private initAtleti():void {
        var eta:number;
        var velocita:number;
        eta = this.generaNumeroCasuale(20,50);
        velocita = this.generaNumeroCasuale(5,6,2);
        this.atleta_1 = new Atleta("Giovanni",eta,velocita,"#gara .atleta1");
        eta = this.generaNumeroCasuale(20,50);
        velocita = this.generaNumeroCasuale(5,6,2);
        this.atleta_2 = new Atleta("Andrea",eta,velocita,"#gara .atleta2");
        eta = this.generaNumeroCasuale(20,50);
        velocita = this.generaNumeroCasuale(5,6,2);
        this.atleta_3 = new Atleta("Anna",eta,velocita,"#gara .atleta3");
        this.atleti = [this.atleta_1,this.atleta_2,this.atleta_3];
    }

    private fillDescrizioni():void {
        for (var i = 0; i < this.atleti.length; i++) {
            var atleta: Atleta = this.atleti[i];
            var div = this.div_descrizioni.find(".atleta" + (i + 1));
            div.html(atleta.getProfile());
        }
    }

    private clickPulsante():void {
        this.interval_id = setInterval(()=>this.updateAtleti(),500);
        this.pulsante.off("click");
    }

    private updateAtleti():void {
        var gara_finita:boolean = false;
        for (var i=0; i<this.atleti.length; i++) {
            this.atleti[i].corri(10);
            if (this.atleti[i].metri_percorsi_totali>=500) {
                gara_finita = true;
            }
        }
        if (gara_finita) {
            clearInterval(this.interval_id);
            this.pulsante.on("click", ()=>this.riparti());
        }
    }

    private riparti():void {
        this.reset();
        this.interval_id = setInterval(() => this.updateAtleti(),500);
        this.pulsante.off("click");
    }

    private reset():void {
        this.initAtleti();
        this.fillDescrizioni();
    }
}

var gara = new Gara();
