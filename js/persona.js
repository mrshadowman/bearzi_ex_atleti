var Persona = /** @class */ (function () {
    function Persona(name, eta) {
        this.name = name;
        this.age = eta;
    }
    Persona.prototype.getProfile = function () {
        var out = this.name + " è una persona di " + this.age + " anni";
        return out;
    };
    return Persona;
}());
//# sourceMappingURL=persona.js.map