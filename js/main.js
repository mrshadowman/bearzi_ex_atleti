console.log("ex 01 L08");
/** funzione ottimizzata per implementare l'ereditarietà
 *  !!!! NON MODIFICARE !!!!
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

// definisco la classe persona
var Persona = /** @class */ (function () {
    /** costruttore
     * contiene le azioni che vogliamo eseguire quando viene creata
     * una nuova istanza di Persona attraverso l'istruzione "new"
     */
    function Persona(name, age) {
        /**
         * salviamo le variabili ricevute dal costruttore
         * in variabili dell'istanza
         */
        this.nome = name;
        this.eta = age;
    }

    /**
     * metodo che restituisce una stringa
     * che descrive le caratteristiche dell'atleta
     */
    Persona.prototype.getProfile = function () {
        var out = this.nome+" è una persona di "+this.eta+" anni";
        return out;
    };
    return Persona;
}());

/**
 *  definisco la classe Atleta
 *  estende Person
 *  utilizzare la classe di esempio fornita,
 *  sostituendo tutte le occorrenze del nome della classe
 */
var Atleta = (function (_super) {
    /* richiamo alla funzione che si occupa di estendere la classe */
    __extends(Atleta, _super);

    /** costruttore
     * è importante che riporti i parametri della classe da cui eredita,
     * ai quali ne possono essere aggiunti altri
     * @param name
     * @param age
     * @param speed
     * @param selector_barra
     * @returns {*|Atleta}
     * @constructor
     */
    function Atleta(name, age, speed, selector_barra) {
        /**
         * chiamata al costruttore della classe padre
         * controllare che i parametri che seguono "this"
         * siano gli stessi previsti dal costruttore della classe padre
         */
        var _this = _super.call(this, name, age) || this;
        // salvo la variabile speed ricevuta nella proprietà "velocita" dell'istanza
        _this.velocita = speed;
        //
        // utilizzo la variabile "selector_barra" per identificare il div della barra
        _this.div_barra = $(selector_barra);
        // dal div della barra identifico il div del nome e dei metri attraverso il meotodo "find"
        _this.div_nome = this.div_barra.find(".nome");
        _this.div_metri = this.div_barra.find(".metri");
        //
        // richiamo la funzione che scrive i valori iniziali dei div
        _this.initDivs();
        //
        return _this;
    }

    /** dichiaro alcune proprietà della classe */
    Atleta.prototype.div_barra;
    Atleta.prototype.div_nome;
    Atleta.prototype.div_metri;
    Atleta.prototype.metri_percorsi_totali = 0;

    /** sovrascrive il metodo della classe padre aggiungendone funzionalità
     */
    Atleta.prototype.getProfile = function () {
        /**
         * richiamo il metodo della classe padre
         * e assegno il valore ad una variabile temporanea
         */
        var out = _super.prototype.getProfile.call(this);
        /**
         * ora posso modificare il risultato ed infine restituirlo
         */
        out += " che corre a "+this.velocita+" metri al secondo";
        return out;
    };

    /**
     * funzione che imposta lo stato iniziale dei div
     * può essere richiamata nel costruttore e ad ogni reset
     */
    Atleta.prototype.initDivs = function () {
        /**
         * attraverso il metodo html di JQuery
         * posso sostituire il contenuto del div
         */
        this.div_nome.html(this.nome);
        this.div_metri.html(this.metri_percorsi_totali+"mt");
        /**
         * reimposto i valori di width e background della barra
         * in modo che al reset non mantengano i valori
         * assegnati da altre funzioni precedentemente
         */
        this.div_barra.css({
            width: "",
            background: ""
        })
    };

    /**
     * funzione che fa avanzare di tot secondi l'atleta
     * @param secondi
     */
    Atleta.prototype.corri = function (secondi) {
        // calcolo i metri percorsi
        var metri_percorsi_adesso = this.velocita*secondi;
        // definisco una variabile dove salvare la perdita di metri
        var perdita_mt = 0;
        // faccio un loop ogni 10 metri finchè non arrivo alla distanza percorsa fin'ora
        var distanza = 0;
        while (distanza<=metri_percorsi_adesso) {
            perdita_mt += (distanza/10)/100*(0.2*this.eta);
            distanza += 10;
        }
        // aggiorno i metri percorsi totali, arrotondando la perdita per evitare che vengano visualizzati numeri con la virgola
        this.metri_percorsi_totali += metri_percorsi_adesso - Math.round(perdita_mt);
        // richiamo la funzione che aggiorna i div della barra di questo atleta
        this.updateBarra();
    };

    /**
     * funzione che aggiorna contenuti e aspetto della barra
     */
    Atleta.prototype.updateBarra = function() {
        // controllo se l'atleta ha raggiunto il traguardo
        if (this.metri_percorsi_totali<500) {
            // aggiorno i metri nella barra
            this.div_metri.html(Math.round(this.metri_percorsi_totali)+"mt");
            // aggiorno la larghezza della barra
            // utilizzando la funzione css di JQuery
            this.div_barra.css({
                width: Math.round(this.metri_percorsi_totali/500*100)+"%"
            });
        } else {
            // imposto l'aspetto finale della barra se l'utente ha raggiunto il traguardo
            this.div_metri.html("500mt");
            this.div_barra.css({
                width: "100%",
                background: "red"
            });
        }

    };

    return Atleta;

    /**
     * !!! IMPORTANTE !!!
     * nelle parentesi di chiusura della definizione della classe
     * va inserito il nome della classe padre
     */
}(Persona));

/**
 * La classe Gara implementa le funzioni che inizialmente erano esterne alle classi
 * in questo modo abbiamo un'istanza unica che gestisce la nostra applicazione
 * e possiamo anche controllarla agevolmente dalla console del browser per il debug
 */
var Gara = (function () {
    function Gara() {
        // assegno subito ad alcune variabili della classe i div che dovrò utilizzare in seguito
        this.div_descrizioni = $("#descrizioni");
        this.pulsante = $("#start input");
        // assegno "this" ad una variabile temporanea "_this"
        // per assicurarmi che la funzione richiamata al click sul pulsante
        // non presenti problemi di scoping
        var _this = this;
        // assegno la funzione al click sul pulsante attraverso il metodo "on" di JQuery
        this.pulsante.on("click",function () {
            _this.clickPulsante();
        });
        // richiamo la funzione che inizializza gli atleti
        this.initAtleti();
        // richiamo la funzione che visualizza le descrizioni degli atleti
        this.fillDescrizioni();
    }
    // dichiaro alcune proprietà della classe
    Gara.prototype.pulsante;
    Gara.prototype.atleta_1;
    Gara.prototype.atleta_2;
    Gara.prototype.atleta_3;
    Gara.prototype.atleti;
    Gara.prototype.div_descrizioni;
    Gara.prototype.interval_id;

    /**
     * funzione che gestisce il primo click sul pulsante start
     */
    Gara.prototype.clickPulsante = function () {
        // variabile temporanea per fixare lo scoping della funzione richiamata dall'intervallo
        var _this = this;
        // imposto l'intervallo con il quale verrà chiamata la funzione updateAtleti a ripetizione
        // salvo il risultato in una variabile che mi servirà
        // per poter interrompere la ripetizione
        this.interval_id = setInterval(function () {
            _this.updateAtleti();
        },500);
        // disabilito il pulsante finchè non finisce la gara
        this.pulsante.off("click");
    };

    /**
     * funzione che inizializza gli atleti
     */
    Gara.prototype.initAtleti = function () {
        // dichiaro alcune variabili temporanee che verranno riutilizzate nella funzione
        var eta,velocita;
        // genero età e velocità per l'atleta
        eta = this.generaNumeroCasuale(20,50);
        velocita = this.generaNumeroCasuale(5,6,2);
        // creo l'atleta
        this.atleta_1 = new Atleta("Giovanni",eta,velocita,"#gara .atleta1");
        // genero età e velocità per l'atleta
        eta = this.generaNumeroCasuale(20,50);
        velocita = this.generaNumeroCasuale(5,6,2);
        // creo l'atleta
        this.atleta_2 = new Atleta("Andrea",eta,velocita,"#gara .atleta2");
        // genero età e velocità per l'atleta
        eta = this.generaNumeroCasuale(20,50);
        velocita = this.generaNumeroCasuale(5,6,2);
        // creo l'atleta
        this.atleta_3 = new Atleta("Anna",eta,velocita,"#gara .atleta3");
        // creo un'array di atleti per gestirli successivamente attraverso dei loop
        this.atleti = [this.atleta_1,this.atleta_2,this.atleta_3];
    };

    /**
     * utility per generare numeri casuali
     * porting dalla funzione creata in precedenza per gli esercizi
     * @param minimo
     * @param massimo
     * @param decimali
     * @returns {number}
     */
    Gara.prototype.generaNumeroCasuale = function (minimo, massimo, decimali) {
        var out = NaN;
        if (typeof decimali === "undefined") {
            decimali = 0;
        }
        var fattore_decimali = Math.pow(10,decimali);
        // calcolo la differenza
        var diff = (massimo*fattore_decimali)-(minimo*fattore_decimali);
        // generiamo un numero casuale
        var numero = Math.random();
        var var_casuale = Math.round(numero*diff);
        // ci assicuriamo che rientri nell'intervallo dato
        // restituiamo il numero
        out = (((minimo*fattore_decimali) + var_casuale)/fattore_decimali);
        return out;
    };

    /**
     * funzione che aggiorna gli atleti ad intervalli regolari
     */
    Gara.prototype.updateAtleti = function () {
        // variabile di controllo per decidere se la gara è finita
        var gara_finita = false;
        // loop tra gli atleti
        for (var i=0; i<this.atleti.length; i++) {
            // lancio la funzione corri
            this.atleti[i].corri(10);
            // controllo i metri percorsi per capire se questo atleta ha finito
            if (this.atleti[i].metri_percorsi_totali>=500) {
                // se almeno un'atleta ha raggiunto il traguardo la gara è finita
                gara_finita = true;
            }
        }
        if (gara_finita) {
            // la gara è finita, interrompo la ripetizione
            clearInterval(this.interval_id);
            // imposto una nuova funzione al click sul pulsante
            var _this = this;
            this.pulsante.on("click", function () {
                _this.riparti();
            });
        }

    };

    /**
     * funzione che fa ripartire la gara dopo la prima volta
     */
    Gara.prototype.riparti = function () {
        // resetto gli elementi
        this.reset();
        // imposto una nuova ripetizione
        var _this = this;
        this.interval_id = setInterval(function () {
            _this.updateAtleti();
        },500);
        // disabilito il pulsante start
        this.pulsante.off("click");
    };

    /**
     * funzione che ripristina lo stato iniziale
     */
    Gara.prototype.reset = function () {
        // ricrea gli atleti in modo casuale
        this.initAtleti();
        // riscrive le descrizioni
        this.fillDescrizioni();
    };

    /**
     * funzione che aggiorna le descrizioni degli atleti
     */
    Gara.prototype.fillDescrizioni = function () {
        // loop tra gli atleti
        for (var i=0; i<this.atleti.length; i++) {
            // variabile temporanea che contiene il singolo atleta durante il ciclo
            var atleta = this.atleti[i];
            // variabile temporanea che identifica il div della descrizione dell'atleta
            var div = this.div_descrizioni.find(".atleta"+(i+1));
            // riscrivo il contenuto del div
            div.html(atleta.getProfile());
        }
    };
    return Gara;
}());

/**
 * creo un'istanza di Gara in modo da far partire l'applicazione
 * senza questa istruzione le definizioni delle classi
 * fatte fin'ora non hanno alcun'effetto
 */
var gara = new Gara();

