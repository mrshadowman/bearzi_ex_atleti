/// <reference path="../dts/jquery/index.d.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Atleta = /** @class */ (function (_super) {
    __extends(Atleta, _super);
    function Atleta(name, age, speed, selector_barra) {
        var _this = _super.call(this, name, age) || this;
        _this.metri_percorsi_totali = 0;
        _this.velocita = speed;
        _this.div_barra = $(selector_barra);
        _this.div_nome = _this.div_barra.find(".nome");
        _this.div_metri = _this.div_barra.find(".metri");
        _this.initDivs();
        return _this;
    }
    Atleta.prototype.initDivs = function () {
        this.div_nome.html(this.name);
        this.div_metri.html(this.metri_percorsi_totali + "mt");
        this.div_barra.css({
            width: "",
            background: ""
        });
    };
    Atleta.prototype.getProfile = function () {
        var out = _super.prototype.getProfile.call(this);
        out += " che corre a " + this.velocita + " metri al secondo";
        return out;
    };
    Atleta.prototype.corri = function (secondi) {
        var metri_percorsi_adesso = this.velocita * secondi;
        var perdita_mt = 0;
        var distanza = 0;
        while (distanza <= metri_percorsi_adesso) {
            perdita_mt += (distanza / 10) / 100 * (0.2 * this.age);
            distanza += 10;
        }
        this.metri_percorsi_totali += metri_percorsi_adesso - Math.round(perdita_mt);
        this.updateBarra();
    };
    Atleta.prototype.updateBarra = function () {
        if (this.metri_percorsi_totali < 500) {
            this.div_metri.html(Math.round(this.metri_percorsi_totali) + "mt");
            this.div_barra.css({
                width: Math.round(this.metri_percorsi_totali / 500 * 100) + "%"
            });
        }
        else {
            // imposto l'aspetto finale della barra se l'utente ha raggiunto il traguardo
            this.div_metri.html("500mt");
            this.div_barra.css({
                width: "100%",
                background: "red"
            });
        }
    };
    return Atleta;
}(Persona));
//# sourceMappingURL=Atleta.js.map